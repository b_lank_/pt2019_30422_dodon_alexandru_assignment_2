package PT2019.Queuing.queuingSystem;

import javax.swing.SwingUtilities;
import PT2019.Queuing.queuingSystem.GUI.MainFrame;

public class App 
{
    public static void main( String[] args )
    {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				@SuppressWarnings("unused")
				MainFrame mf = new MainFrame();
			}
		});
    }
}
