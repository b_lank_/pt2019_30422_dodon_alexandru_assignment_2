package PT2019.Queuing.queuingSystem.GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.*;

import PT2019.Queuing.queuingSystem.Simulation.Manager;
import PT2019.Queuing.queuingSystem.Simulation.Server;

public class MainFrame extends JFrame {
	
	private final JButton start = new JButton("Start Simulation");
	private final JTextField maxClients = new JTextField();
	private final JTextField minTClients = new JTextField();
	private final JTextField maxTClients = new JTextField();
	private final JTextField minTService = new JTextField();
	private final JTextField maxTService = new JTextField();	
	private final JTextField endTime = new JTextField();
	private final JTextField intervalStart = new JTextField();
	private final JTextField intervalEnd = new JTextField();
	private final JTextField noQ = new JTextField();
	private String[] strats = {"Shortest waiting time", "Shortest queue"};
	private final JComboBox<String> strat = new JComboBox<String>(strats);
	private JScrollPane[] displayQueues;
	private ArrayBlockingQueue<String> events;	
	private String eventsText;
	private JScrollPane eventsLog = new JScrollPane();
	private  JPanel display = new JPanel();
	private JLabel[] out = new JLabel[10];
	private boolean canUpdate = true;
	JPanel datap = new JPanel();
	
    public MainFrame() {    	
		this.setTitle("Queue systems simulation");
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        
        
		this.setLayout(new GridBagLayout());
		
		JPanel left = new JPanel();
		left.setLayout(new GridBagLayout());
		{
			JPanel left1 = new JPanel();
			left1.setLayout(new GridBagLayout());
			{
				JLabel l1 = new JLabel("Minimum interval between clients");
				MainFrame.addGUI(left1, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l1);
				
				//JTextField minTClients
				MainFrame.addGUI(left1, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, minTClients);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 1, 1, 1, 0, 0, left1);
			
			JPanel left2 = new JPanel();
			left2.setLayout(new GridBagLayout());
			{
				JLabel l2 = new JLabel("Maximum interval between clients");
				MainFrame.addGUI(left2, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l2);

				//JTextField maxTClients
				MainFrame.addGUI(left2, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, maxTClients);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 2, 1, 1, 0, 0, left2);
			
			JPanel left3 = new JPanel();
			left3.setLayout(new GridBagLayout());
			{
				JLabel l3 = new JLabel("Minimum service time");
				MainFrame.addGUI(left3, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l3);
				
				//JTextField minTService
				MainFrame.addGUI(left3, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, minTService);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 3, 1, 1, 0, 0, left3);
			
			JPanel left4 = new JPanel();
			left4.setLayout(new GridBagLayout());
			{
				JLabel l4 = new JLabel("Maximum service time");
				MainFrame.addGUI(left4, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l4);
				
				//JTextField maxTService
				MainFrame.addGUI(left4, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, maxTService);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 4, 1, 1, 0, 0, left4);
			
			JPanel left5 = new JPanel();
			left5.setLayout(new GridBagLayout());
			{
				JLabel l5 = new JLabel("End time");
				MainFrame.addGUI(left5, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l5);
				
				//JTextField endTime
				MainFrame.addGUI(left5, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, endTime);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 5, 1, 1, 0, 0, left5);
			
			JPanel left6 = new JPanel();
			left6.setLayout(new GridBagLayout());
			{
				JLabel l6 = new JLabel("Interval start");
				MainFrame.addGUI(left6, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l6);
				
				//JTextField intervalStart
				MainFrame.addGUI(left6, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, intervalStart);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 6, 1, 1, 0, 0, left6);
			
			JPanel left7 = new JPanel();
			left7.setLayout(new GridBagLayout());
			{
				JLabel l7 = new JLabel("Interval end");
				MainFrame.addGUI(left7, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l7);
				
				//JTextField intervalEnd
				MainFrame.addGUI(left7, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, intervalEnd);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 7, 1, 1, 0, 0, left7);
			
			JPanel left8 = new JPanel();
			left8.setLayout(new GridBagLayout());
			{
				JLabel l8 = new JLabel("Number of Queues");
				MainFrame.addGUI(left8, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l8);
				
				//JTextField noQ
				MainFrame.addGUI(left8, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, noQ);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 8, 1, 1, 0, 0, left8);
			
			JPanel left9 = new JPanel();
			left9.setLayout(new GridBagLayout());
			{
				JLabel l9 = new JLabel("Queue strategy");
				MainFrame.addGUI(left9, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l9);
				
				//JComboBox strat
				MainFrame.addGUI(left9, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, strat);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 9, 1, 1, 0, 0, left9);

			JPanel left10 = new JPanel();
			left10.setLayout(new GridBagLayout());
			{
				JLabel l10 = new JLabel("Maximum number of clients");
				MainFrame.addGUI(left10, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, l10);
				
				//JButton start
				MainFrame.addGUI(left10, GridBagConstraints.HORIZONTAL, 0, 1, 1, 1, 1, 1, maxClients);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 0, 1, 1, 0, 0, left10);
			
			JPanel left11 = new JPanel();
			left11.setLayout(new GridBagLayout());
			{
				//JButton start
				MainFrame.addGUI(left11, GridBagConstraints.HORIZONTAL, 0, 0, 1, 1, 0, 0, start);
			}
			MainFrame.addGUI(left, GridBagConstraints.BOTH, 0, 10, 1, 1, 0, 0, left11);		
		}
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.insets = new Insets(10, 10, 10, 10);
		add(left, c);		
    	out[0] = new JLabel("Simulation metrics:");
    	out[1] = new JLabel("  Average waiting time");
    	out[2] = new JLabel("  Average service time");
    	out[3] = new JLabel("  Average empty Queue time");
    	out[4] = new JLabel("  Peak time");
    	out[5] = new JLabel("Interval metrics:");
    	out[6] = new JLabel("  Average waiting time");
    	out[7] = new JLabel("  Average service time");
    	out[8] = new JLabel("  Average empty Queue time");
    	out[9] = new JLabel("  Peak time");
		
		final MainFrame ref = this;
		
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				try {
					int mtc = Integer.parseInt(minTClients.getText());
					int Mtc = Integer.parseInt(maxTClients.getText());
					int mts = Integer.parseInt(minTService.getText());
					int Mts = Integer.parseInt(maxTService.getText());
					int et = Integer.parseInt(endTime.getText());
					int is = Integer.parseInt(intervalStart.getText());
					int ie = Integer.parseInt(intervalEnd.getText());
					int nq = Integer.parseInt(noQ.getText());
					int mc = Integer.parseInt(maxClients.getText());

			    	events = new ArrayBlockingQueue<String>(20);

					Manager m = new Manager(ref, events, mtc, Mtc, mts, Mts, et, is, ie, nq, mc, (String) strat.getSelectedItem());
					new Thread(m).start();
				} 
				catch (Exception ex){
					System.out.println("Parameter input error\n");
					ex.printStackTrace();
					start.setEnabled(true);
				}
			}
		});
		
		this.pack();
		this.setVisible(true);
    }
	
	private static void addGUI(JComponent destination, int GridBagConstraints, int gx, int gy, int gh, int gw, float px, float py, JComponent toAdd) {
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints;
		c.gridx = gx;
		c.gridy = gy;
		c.gridheight = gh;
		c.gridwidth = gw;
		c.weightx = px;
		c.weighty = py;
		destination.add(toAdd, c);
	}
	
	private void makeLog() {
		while (events.isEmpty() == false) {
			try {
				eventsText += events.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
    
    public void updateDisplay(ArrayList<Server> servers) {
    	if (canUpdate) {
        	makeLog();
        	JTextArea t = new JTextArea(eventsText, 20, 20);
        	t.setEditable(false);
        	eventsLog.setViewportView(t);
        	
        	for(int i = 0, n = servers.size(); i < n; i++) {
        		displayQueues[i].setViewportView(servers.get(i).getClients());
        	}

        	this.pack();
    	}
    }
    
    public void startOfSimulation(int q) {
    	canUpdate = true;
    	maxClients.setEnabled(false);
    	minTClients.setEnabled(false);
    	maxTClients.setEnabled(false);
    	minTService.setEnabled(false);
    	maxTService.setEnabled(false);
    	endTime.setEnabled(false);
    	intervalStart.setEnabled(false);
    	intervalEnd.setEnabled(false);
    	noQ.setEnabled(false);
    	strat.setEnabled(false);
		start.setEnabled(false);
		
		eventsText = "Event Log:\n";
		
		display.removeAll();
		display.setLayout(new GridBagLayout());
		displayQueues = new JScrollPane[q];
    	for(int i = 0; i < q; i++) {
    		JLabel label = new JLabel("Queue #" + Integer.toString(i + 1) + " ");
    		displayQueues[i]= new JScrollPane();
    		displayQueues[i].setEnabled(false);
    		MainFrame.addGUI(display, GridBagConstraints.HORIZONTAL, i, 0, 1, 1, 1, 0, label);
    		MainFrame.addGUI(display, GridBagConstraints.BOTH,       i, 1, 1, 1, 1, 1, displayQueues[i]);
    	}
    	datap.removeAll();
    	datap.setLayout(new GridBagLayout());
    	for (int i = 0; i < 10; i++) {
        	MainFrame.addGUI(datap, GridBagConstraints.HORIZONTAL, 0, i, 1, 1, 1, 1, out[i]);
    	}

    	out[0].setText("Simulation metrics:");
    	out[1].setText("  Average waiting time");
    	out[2].setText("  Average service time");
    	out[3].setText("  Average empty Queue time");
    	out[4].setText("  Peak time");
    	out[5].setText("Interval metrics:");
    	out[6].setText("  Average waiting time");
    	out[7].setText("  Average service time");
    	out[8].setText("  Average empty Queue time");
    	out[9].setText("  Peak time");
    	
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.insets = new Insets(10, 10, 10, 10);
		add(datap, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.insets = new Insets(10, 10, 10, 10);
		add(eventsLog, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.insets = new Insets(10, 10, 10, 10);
		add(display, c);
    }
   
    public void endOfSimulation(float[] data) {
    	canUpdate = false;
    	maxClients.setEnabled(true);
    	minTClients.setEnabled(true);
    	maxTClients.setEnabled(true);
    	minTService.setEnabled(true);
    	maxTService.setEnabled(true);
    	endTime.setEnabled(true);
    	intervalStart.setEnabled(true);
    	intervalEnd.setEnabled(true);
    	noQ.setEnabled(true);
    	strat.setEnabled(true);
		start.setEnabled(true);
		
    	out[1].setText("  Average waiting time: "     + data[0]);
    	out[2].setText("  Average service time: "     + data[1]);
    	out[3].setText("  Average empty Queue time: " + data[2]);
    	out[4].setText("  Peak time: "                + (int)data[3]);
    	out[6].setText("  Average waiting time: "     + data[4]);
    	out[7].setText("  Average service time: "     + data[5]);
    	out[8].setText("  Average empty Queue time: " + data[6]);
    	out[9].setText("  Peak time: "                + (int)data[7]);
    }
}
