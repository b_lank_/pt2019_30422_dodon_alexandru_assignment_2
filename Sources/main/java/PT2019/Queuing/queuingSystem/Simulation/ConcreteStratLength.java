package PT2019.Queuing.queuingSystem.Simulation;

import java.util.ArrayList;

public class ConcreteStratLength implements Strategy {

	public void addClient(ArrayList<Server> servers, Client c) {
		Server minS = servers.get(0);
		int minL = minS.getNoInQueue();
		
		for (Server i : servers) {
			if (minL > i.getNoInQueue()) {
				minS = i;
				minL = i.getNoInQueue();
			}
		}
		
		minS.addClient(c);
	}

}
