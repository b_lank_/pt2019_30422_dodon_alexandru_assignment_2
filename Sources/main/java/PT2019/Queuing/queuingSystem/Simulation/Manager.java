package PT2019.Queuing.queuingSystem.Simulation;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

import PT2019.Queuing.queuingSystem.GUI.MainFrame;

public class Manager implements Runnable {

	
	private ArrayList<Client> generatedClients;
	private Scheduler scheduler;
	private MainFrame mf;
	private ArrayBlockingQueue<String> events;
	private int minTClients;
	private int maxTClients;
	private int minTService;
	private int maxTService;
	private int currentTime;
	private int endTime;
	private int intervalStart;
	private int intervalEnd;
	private int numberQ;
	private int maxClients;
	private float [] data;
	private float peak;
	private float ipeak;
	
	public Manager(MainFrame mf, 
ArrayBlockingQueue<String> ev, 
int minTClients, int maxTClients, 
int minTService,
int maxTService, int endTime, 
int intervalStart, int intervalEnd, 
int noQ, int maxClients, String strat) {
		this.mf = mf;
		events = ev;
		this.minTClients = minTClients;
		this.maxTClients = maxTClients;
		this.minTService = minTService;
		this.maxTService = maxTService;
		currentTime = 0;
		this.endTime = endTime;
		this.intervalStart = intervalStart;
		this.intervalEnd = intervalEnd;
		numberQ = noQ;
		this.maxClients = maxClients;
		data = new float[8];
		peak = 0;
		ipeak = 0;

		scheduler = new Scheduler(numberQ, 20, ev);
		if (strat.compareTo("Shortest waiting time") == 0) {
			scheduler.selectStrat(SelectionPolicy.shortestTime);
		} else {
			scheduler.selectStrat(SelectionPolicy.shorestQueue);
		}
		
		generatedClients = new ArrayList<Client>();
		this.generateRandomClients();
	}
	
	private void generateRandomClients() {
		Random random = new Random();
		int currentTime = random.nextInt(maxTClients);
		int k = 0;
		
		while (currentTime < endTime && k < maxClients) {
			generatedClients.add(new Client(currentTime, random.nextInt(maxTService - minTService + 1) + minTService, ++k));
			currentTime += random.nextInt(maxTClients - minTClients + 1) + minTClients;
		}
	}

	public float[] getData() {
		data[2] /= scheduler.getServers().size();
		data[2] = (int)(data[2] * 100) / (float)100;
		data[3] = peak;
		data[6] /= scheduler.getServers().size();
		data[6] = (int)(data[6] * 100) / (float)100;
		data[7] = ipeak;
		
		int n = 0;
		int m = 0;
		for (Server i : scheduler.getServers()) {
			ArrayList<Client> clientList = i.getClientsData();
			n += clientList.size();
			
			for (Client j : clientList) {
				data[0] += j.getWaitingTime();
				data[1] += j.getProcessingTime();
				
				if (intervalStart <= j.getArrivalTime() && j.getArrivalTime() <= intervalEnd) {
					data[4] += j.getWaitingTime();
					data[5] += j.getProcessingTime();
					m += 1;
				}
			}
		}

		data[0] /= n;
		data[0] = (int)(data[0] * 100) / (float)100;
		data[1] /= n;
		data[1] = (int)(data[1] * 100) / (float)100;
		data[4] /= m;
		data[4] = (int)(data[4] * 100) / (float)100;
		data[5] /= m;
		data[5] = (int)(data[5] * 100) / (float)100;
		
		return data;
	}
	
	private void gatherData() {
		int aux = 0, iaux = 0;
		for (Server i : scheduler.getServers()) {
			int c = i.getMomentaryClientNumber();
			if (c == 0) {
				data[2]++;
				
				if (intervalStart <= currentTime && currentTime <= intervalEnd) {
					data[6]++;
				}
			}
			
			aux += c;
			if (intervalStart <= currentTime && currentTime <= intervalEnd) {
				iaux += c;
			}
		}
		
		if (peak < aux) {
			peak = currentTime;
		}
		
		if (ipeak < iaux) {
			ipeak = currentTime;
		}
	}
	
	public void run() {
		mf.startOfSimulation(numberQ);
		boolean run = true;
		int c = 0;
		while (run) {
			try {
				events.put("\nTime unit  " + currentTime +":\n");
				
				if (currentTime <= endTime) {
					while (generatedClients.isEmpty() == false && currentTime == generatedClients.get(0).getArrivalTime()) {
						events.put("    C#" + generatedClients.get(0).getID() + " has arrived!\n");
						scheduler.dispatchClient(generatedClients.get(0));
						generatedClients.remove(0);
					}
				} else {
					c = 0;
					for (Server i : scheduler.getServers()) {
						c += i.getMomentaryClientNumber();
					}
					
					if (c == 0) {
						run = false;
					}
				}

				currentTime++;
				for (int i = 0; i < 5; i++) {
					mf.updateDisplay(scheduler.getServers());
					Thread.sleep(200);
				}
				gatherData();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		scheduler.killThreads();
		mf.endOfSimulation(getData());
	}

}
