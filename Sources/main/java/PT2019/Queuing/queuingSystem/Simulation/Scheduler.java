package PT2019.Queuing.queuingSystem.Simulation;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

import PT2019.Queuing.queuingSystem.GUI.MainFrame;

public class Scheduler {
	private ArrayList<Server> servers;
	private int noServers;
	private int maxClientsPerServer;
	private Strategy strat;
	
	
	public Scheduler(int s, int maxClients, ArrayBlockingQueue<String> ev) {
		servers = new ArrayList<Server>();
		noServers = s;
		maxClientsPerServer = maxClients;
		
		for (int i = 0; i < noServers; i++) {
			servers.add(new Server(maxClientsPerServer, i + 1, ev));
		}
		
		for (int i = 0; i < noServers; i++) {
			new Thread(servers.get(i)).start();
		}
	}
	
	public void killThreads() {
		for (Server i : servers) {
			i.kill();
		}
	}
	
	public ArrayList<Server> getServers() {
		return servers;
	}
	
	public void selectStrat(SelectionPolicy policy) {
		if (policy == SelectionPolicy.shortestTime) {
			strat = new ConcreteStratTime();
		} else if (policy == SelectionPolicy.shorestQueue) {
			strat = new ConcreteStratLength();
		}
	}
	
	public void dispatchClient(Client c) {
		strat.addClient(servers, c);
	}
}
