package PT2019.Queuing.queuingSystem.Simulation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

import PT2019.Queuing.queuingSystem.GUI.MainFrame;

public class Server  implements Runnable{
	private ArrayBlockingQueue<Client> clients;
	private AtomicInteger waitingPeriod;
	private AtomicInteger noInQueue;
	private ArrayBlockingQueue<String> events;
	private int id;
	private boolean run;
	private ArrayList<Client> forData;
		
	public Server(int size, int id, ArrayBlockingQueue<String> ev) {
		clients = new ArrayBlockingQueue<Client>(size);
		waitingPeriod = new AtomicInteger(0);
		noInQueue = new AtomicInteger(0);
		this.id = id;
		events = ev;
		run = true;
		forData = new ArrayList<Client>();
	}
	
	public void addClient(Client newClient) {
		try {
			clients.put(newClient);
			events.put("    C#" + newClient.getID() + " started waiting in Q#" + id + "\n");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		newClient.setWaitingTime(this.getWaitingPeriod());
		forData.add(newClient);
		
		waitingPeriod.addAndGet(newClient.getProcessingTime());
		noInQueue.addAndGet(1);
	}
	
	public JList<String> getClients() {
		DefaultListModel<String> res = new DefaultListModel<String>();
		
		Iterator<Client> i = clients.iterator(); 
		while (i.hasNext()) {
			res.addElement("Client number #" + Integer.toString(i.next().getID()));
		}
		if(res.isEmpty()) {
			res.addElement("        Empty        ");
		}
		return new JList<String>(res);
	}
	
	public int getMomentaryClientNumber() {
		return clients.size();
	}
	
	public ArrayList<Client> getClientsData() {
		return forData;
	}

	public int getWaitingPeriod() {
		return waitingPeriod.get();
	}

	public int getNoInQueue() {
		return noInQueue.get();
	}
	
	public void kill() {
		run = false;
	}
	
	public void run() {
		Client current = null;
		while (run) {
			try {
				if (clients.isEmpty() == false) {
					current = clients.peek();
					events.put("    Q#" + id + " is now processing C#" + current.getID() + "\n");
					Thread.sleep(current.getProcessingTime() * 1000);
					waitingPeriod.addAndGet(-1 * current.getProcessingTime());
					noInQueue.addAndGet(-1);
					current = clients.take();
					events.put("    Q#" + id + " is done processing C#" + current.getID() + "\n");
				} 
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
