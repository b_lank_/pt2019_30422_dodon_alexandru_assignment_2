package PT2019.Queuing.queuingSystem.Simulation;

import java.util.ArrayList;

public class ConcreteStratTime implements Strategy {

	public void addClient(ArrayList<Server> servers, Client c) {
		Server minS = servers.get(0);
		int minT = minS.getWaitingPeriod();
		
		for (Server i : servers) {
			if (minT > i.getWaitingPeriod()) {
				minS = i;
				minT = i.getWaitingPeriod();
			}
		}
		
		minS.addClient(c);
	}

}
