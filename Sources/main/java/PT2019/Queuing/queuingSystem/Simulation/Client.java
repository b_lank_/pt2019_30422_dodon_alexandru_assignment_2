package PT2019.Queuing.queuingSystem.Simulation;

public class Client {
	private int arrivalTime;
	private int processingTime;
	private int id;
	private int waitingTime;
	
	public Client(int at, int pt, int id) {
		arrivalTime = at;
		processingTime = pt;
		this.id = id;
	}
	
	public void setWaitingTime(int t) {
		waitingTime = t;
	}
	
	public int getWaitingTime() {
		return waitingTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public int getID() {
		return id;
	}
}
